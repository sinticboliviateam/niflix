# -*- coding: utf-8 -*-
import requests
from lxml import html
import json
import urllib
import os
import re
import time


class PelisGratis:
    websiteUrl = ''
    response = None
    responseHeaders = None
    responseCookies = None
    lastUrl = None
    
    def __init__(self):
        # self.websiteUrl = 'https://mrpelis.tv/pelisgratis/' 
        self.websiteUrl = 'https://pelisgratis.me'

    """
    Sends a web requests
    """
    def webRequest(self, url, type='GET', data={}, returnType='tree',
                    parameters=None, headers=None, cookies=None):

        #headers = {'DNT': '1'}
        if headers is None:
            headers = {'Referer': self.lastUrl}
        else:
            headers['Referer'] = self.lastUrl
            
        print 'SENDING HEADERS TO => {0}'.format(url)
        print headers
        if type == 'POST':
            #print data
            self.response = requests.post(url, data, headers=headers, cookies=cookies,
                allow_redirects=True
            )
        elif type == 'PUT':
            self.response = requests.put(url, headers=headers, cookies=cookies,
                allow_redirects=True
            )
        else:
            self.response = requests.get(url, headers=headers, cookies=cookies,
                allow_redirects=True,
                timeout=1000
            )
        self.lastUrl = url
        # save response headers
        self.responseHeaders = self.response.headers
        # save response cookies
        self.responseCookies = self.response.cookies
        #print self.response.content
        if returnType == 'tree':
            #print 'RETURNING (TREE)'
            if self.response is None or self.response.content is None:
                return None
            #vpLog("Tree");
            #vpLog('Encoding: {0}'.format(page.encoding))
            rawHtml = self.response.content.strip() #decode('UTF-8')
            #vpLog(rawHtml)
            tree = None
            if rawHtml.find('<body') == -1:
                rawHtml = '<html><head><title>Dummy HTML</title></head><body>{0}</body></html>'.format(rawHtml)
                #tree = html.fragment_fromstring(rawHtml, create_parent='div')
                tree = html.fromstring(rawHtml)
            else:
                tree = html.fromstring(rawHtml)
            return tree

        return self.response.content

    def showCookies(self, jar):
        print 'Showing JAR Cookies'
        for cookie, value in jar.items():
            print '{0} => {1}'.format(cookie, value)

    def showResponseCookies(self):
        self.showCookies(self.responseCookies)

    def getCategories(self):
        categories = []
        tree = self.webRequest(self.websiteUrl)
        items = tree.xpath('//div[@id="categories-2"]/ul/li')
        for item in items:
            node = item.xpath('.//a')[0]
            url = node.get('href')
            name = unicode(node.text)
            category = {'name': name, 'url': url}
            categories.append(category)

        return categories

    def getLatestMovies(self, page=1):
        if page <= 0:
            page = 1
        url = self.websiteUrl
        if page > 1:
            url += '/page/{0}'.format(page)
        print url
        tree = self.webRequest(url)
        movies = self.extractMovies(tree)
        #pages = tree.xpath('count(//div[@class="wp-pagenavi"]/a)')
        pageNodes = tree.xpath('//div[@class="wp-pagenavi"]/a')
        pages = int(pageNodes[-2].text.strip())
        return {'movies': movies, 'pages': pages}

    def getCategoryMovies(self, url, page=1):
        if page <= 0:
            page = 1
        categoryUrl = url
        if page > 1:
            categoryUrl += 'page/{0}/'.format(page)
        tree = self.webRequest(categoryUrl)
        movies = self.extractMovies(tree)
        pageNodes = tree.xpath('//div[@class="wp-pagenavi"]/a')
        pages = int(pageNodes[-2].text.strip())

        return {'movies': movies, 'pages': pages}

    def extractMovies(self, tree):
        movies = []
        lists = tree.xpath('//ul[contains(@class, "MovieList")]')

        items = lists[0].xpath('.//li')

        for item in items:
            #qualityNode = item.xpath('.//article/div/p/span[4]')
            #qualityNode[0].text if qualityNode is not None else ''
            #print qualityNode
            quality = ''
            image_url = item.xpath('.//article//img')[0].get('src')
            if image_url.find('http') == -1:
                image_url = 'http:' + image_url

            imageFile = os.path.basename(image_url)
            imageBaseUrl = os.path.dirname(image_url)
            image_url = u'{0}/{1}'.format(unicode(imageBaseUrl),
                                        unicode(urllib.quote(imageFile.encode('utf-8')))
            )
            description = unicode(item.xpath('.//article/div/div[@class="Description"]/p[1]')[0].text)
            genre = unicode(item.xpath('.//article/div/div[@class="Description"]/p[2]/a[1]')[0].text)
            actors = ''
            actorNodes = item.xpath('.//article/div/div[@class="Description"]/p[3]/a')
            for actorNode in actorNodes:
                actors += actorNode.text + ','
            actors = actors.strip(',')
            spanNode = item.xpath('.//article/a/span')
            year = spanNode[0].text.strip() if spanNode is not None and len(spanNode) > 0 else ''
            movie = {
                'title': unicode(item.xpath('.//article/a/h3')[0].text).strip(),
                'url': item.xpath('.//article/a')[0].get('href'),
                'image_url': image_url,
                'year': year,
                'duration': item.xpath('.//article/div/p/span[2]')[0].text,
                'quality': quality,
                'description': description +
                                u"\nActores:{0}\nGenero: {1}\nAño:{2}".format(actors, genre, year),
                'genre':  genre,
                'actors': actors
            }

            movies.append(movie)

        return movies

    def getMovieServers(self, url):
        tree = self.webRequest(url, cookies=self.responseCookies)
        self.showResponseCookies()
        print 'RESPONSE HEADERS'
        print self.responseHeaders
        # save cookies
        cookies = dict()
        for cookie, value in self.responseCookies.items():
            cookies[cookie] = value

        #print html.tostring(tree)
        iframe = tree.xpath('//div[contains(@class, "TPlayerTb")]/iframe')[0]
        iframeUrl0 = iframe.get('src')
        print 'IFrame URL: {0}'.format(iframeUrl0)

        #request
        tree1 = self.webRequest(iframeUrl0, cookies=cookies)
        self.showResponseCookies()
        print 'RESPONSE HEADERS'
        print self.responseHeaders
        # save cookies
        for cookie, value in self.responseCookies.items():
            cookies[cookie] = value

        iframe = tree1.xpath('//iframe')[0]
        iframeUrl = iframe.get('src')
        #'''
        host = 'repro.live'
        if 'itatroniks.com' in iframeUrl:
            host = 'itatroniks.com'

        #host = 'pelisgratis.me'
        headers = {}  # self.responseHeaders
        headers['Accept'] ='text/html,application/xhtml+xml,aplication/xml;q=0.9,*/*;q=0.8'
        headers['Accept-Encoding'] = 'gzip, deflate, br'
        headers['Accept-Language'] = 'en-US,en;q=0.5'
        headers['Cache-Control'] = 'no-cache'
        headers['Connection'] = 'keep-alive'
        headers['Content-Type'] = 'text/html; charset=UTF-8'
        headers['Host'] = host
        headers['Referer'] = iframeUrl0
        headers['Pragma'] = 'no-cache'
        headers['Upgrade-Insecure-Requests'] = '1'
        headers['User-Agent'] = 'Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:63.0) Gecko/20100101 Firefox/63.0'
        headers['TE'] = 'Trailers'
        #'''
        print iframeUrl
        print headers
        print self.showCookies(cookies)
        tree1 = self.webRequest(iframeUrl, headers=headers, cookies=cookies)
        print html.tostring(tree1)
        #get server buttons
        buttons = tree1.xpath('//div[contains(@class, "btn-embed")]')
        print buttons
        servers = []
        for btn in buttons:
            embedCode = btn.get('data-embed')
            print embedCode
            serverName = u'{0}'.format(btn.text)
            servers.append({
                'name': serverName,
                'lang': 'Unknow',
                'embed': embedCode
            })
        print servers
        '''



        serversTree = self.webRequest(iframeServersUrl, headers = headers)
        #print html.tostring(serversTree)
        servers = []

        if 'repros.live' in host:
            serverItems = serversTree.xpath('//ul[@class="menuPlayer"]/li')
            for server in serverItems:
                #print html.tostring(server)
                if server.get('title') is None:
                    continue
                #print server.get('data-embed')
                _server_ = {'name': server.get('title'),
                    'embed': server.get('data-embed'),
                    'lang': 'Unknow',
                    'type': 'repros.live'
                }
                #save the server into list
                servers.append(_server_)
                #if 'RapidVideo' in _server_['name']:
                #   rapidVideo = _server_
        else:
            buttons = serversTree.xpath('//div[contains(@class, "modal-content")]/div/center//button')

            #print embedId
            for btn in buttons:
                #print html.tostring(btn)
                _server_ = {
                    'name': unicode(btn.text),
                    'lang': 'Unknow',
                    'type': 'itatroniks.com',
                    'embed': btn.get('id'),
                    'embed_url': iframeServersUrl
                }
                servers.append(_server_)
        #rapidVideo = None
        #print servers
        #if rapidVideo is not None:
        #    print self.getMovieLink(rapidVideo)
        return servers
        '''

    def getMovieLink(self, server):
        _json_ = ''
        serverData = None

        print server

        if server['type'] == 'repros.live':
            url = 'https://repros.live/player/ajaxdata'
            headers = {
                'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8'
            }
            _json_ = self.webRequest(
                url,
                'POST',
                {'codigo': server['embed']},
                'raw',
                headers = headers
            )
            serverData = json.loads(_json_)
        elif server['type'] == 'repro.live':
            url = 'https://repro.live/ajax_embed/iframe/'
            headers = {
                'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8',
                'Host': 'repro.live',
                'Referer': 'https://repro.live/embed/?id=' + server['embed_id'],
                'User-Agent': 'Mozilla/5.0 (X11; Linux x86_64) Gecko/20100101 Firefox/60.0',
                'X-Requested-With': 'XMLHttpRequest'
            }
            _json_ = self.webRequest(
                url,
                'POST',
                {'codigo': server['embed']},
                'raw',
                headers=headers
            )
            print _json_
            serverData = json.loads(_json_)
            serverData['url'] = serverData['link'] if serverData['link'] is not None else serverData['url']
        else:
            number = server['embed_url'].split('/')[-1]
            url = 'https://itatroniks.com/get/{0}/{1}'.format(number, server['embed'])
            headers = {}
            _json_ = self.webRequest(url, 'GET', {}, 'raw', headers=headers)
            serverData = json.loads(_json_)
            serverData['url'] = 'https://rapidvideo.com/e/' + serverData['extid']
            serverData['link'] = None

        print serverData
        movieLink = None
        for q in ['720p', '480p', '360p']:
            #qurl = serverData['url'] + '&q={0}'.format(q)
            qurl = ''
            if serverData['link'] is not None:
                qurl = serverData['link']
            else:
                qurl = serverData['url'].replace('/e/', '?v=') + '&q={0}'.format(q)

            print qurl
            serverTree = self.webRequest(qurl)
            #print html.tostring(serverTree)
            videoNode = serverTree.xpath('//video')[0]
            #print html.tostring(videoNode)
            link = videoNode.get('src')
            if link is None:
                movieSource = videoNode.xpath('.//source')
                #print movieSource
                if movieSource is not None and len(movieSource) > 0:
                    link = movieSource[0].get('src')
            if link is not None:
                movieLink = link
                #print qurl
                break

        #print movieLink
        return movieLink

    def searchMovie(self, keyword):
        url = self.websiteUrl + '/?s=' + keyword
        tree = self.webRequest(url)
        items = self.extractMovies(tree)

        return items

    def getMovieServersOld(self, url):
        import urlparse

        tree = self.webRequest(url)
        #print html.tostring(tree)
        iframes = tree.xpath('//iframe')
        #print 'IFrames found: {0}'.format(len(iframes))
        #print html.tostring(iframes[0])
        iframeUrl = iframes[0].get('src')
        print iframeUrl
        #request
        tree1 = self.webRequest(iframeUrl, headers={
            'Host': 'repro.live',
            'Referer':url, 
            'Upgrade-Insecure-Requests':'1',
            'User-Agent': 'Mozilla/5.0 (X11; Linux x86_64…) Gecko/20100101 Firefox/60.0',
            # ~ 'Accept': 'text/html,application/xhtml+xm…plication/xml;q=0.9,*/*;q=0.8',
            # ~ 'Connection': 'keep-alive',
            # ~ 'Cache-Control': 'max-age=0'
        })
        #print html.tostring(tree1)
        iframes = tree1.xpath('//iframe')
        coms = urlparse.urlparse(iframeUrl)
        host = coms.netloc
        serversTree = None
        print coms
        print 'Host: {0}'.format(host)
        print iframes
        if len(iframes) > 0:
            iframeServersUrl = iframes[0].get('src')
            print iframeServersUrl
            coms = urlparse.urlparse(iframeServersUrl)
            host = coms.netloc
            print coms
             # host = 'repros.live'
            #if 'itatroniks.com' in iframeServersUrl:
            #    host = 'itatroniks.com'

            headers = {
                'content-type': 'text/html; charset=UTF-8',
                'Host': host,
                'Referer': iframeServersUrl,
                'Upgrade-Insecure-Requests': '1',
                'User-Agent': 'Mozilla'
            }
            serversTree = self.webRequest(iframeServersUrl, headers=headers)
            #print html.tostring(serversTree)
        else:
            serversTree = tree1
                   
        servers = []

        if host == 'repros.live':
            serverItems = serversTree.xpath('//ul[@class="menuPlayer"]/li')
            for server in serverItems:
                #print html.tostring(server)
                if server.get('title') is None:
                    continue
                #print server.get('data-embed')
                _server_ = {
                    'name': server.get('title'),
                    'embed': server.get('data-embed'),
                    'lang': 'Unknow',
                    'type': host
                }
                #save the server into list
                servers.append(_server_)
                #if 'RapidVideo' in _server_['name']:
                #   rapidVideo = _server_
        elif host == 'repro.live':
            serverItems = serversTree.xpath('//div[contains(@class, "btn-embed")]')
            for server in serverItems:
                #print html.tostring(server)
                if server.text is None:
                    continue

                _server_ = {
                    'name': server.text.strip(),
                    'embed': server.get('data-embed'),
                    'embed_id': coms.query.split('=')[1],
                    'lang': 'Unknow',
                    'type': host
                }
                #save the server into list
                servers.append(_server_)
        else:
            buttons = serversTree.xpath('//div[contains(@class, "modal-content")]/div/center//button')

            #print embedId
            for btn in buttons:
                #print html.tostring(btn)
                _server_ = {
                    'name': unicode(btn.text),
                    'lang': 'Unknow',
                    'type': 'itatroniks.com',
                    'embed': btn.get('id'),
                    'embed_url': iframeServersUrl
                }
                servers.append(_server_)
        #rapidVideo = None
        #print servers
        #if rapidVideo is not None:
        #    print self.getMovieLink(rapidVideo)
        return servers


if __name__ == '__main__':
    obj = PelisGratis()
    #movies = obj.getLatestMovies(page=1)
    #print movies
    #movies = obj.searchMovie('los increibles')
    #print movies
    #print obj.getCategories()
    #servers = obj.getMovieServersOld('https://pelisgratis.me/cementerio-maldito/')
    #servers = obj.getMovieServers('https://pelisgratis.me/los-increibles-2-b/')
    #servers = obj.getMovieServersOld('https://pelisgratis.me/creed-2-a/')
    servers = obj.getMovieServersOld('https://pelisgratis.me/la-ultima-jugada/')
    print 'Servers'
    print servers
    print obj.getMovieLink(servers[3])
    #obj.getMovieData('https://pelisgratis.tv/?trembed=0&trid=9300')
