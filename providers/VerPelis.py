# -*- coding: utf-8 -*-
import os,sys,inspect
currentdir = os.path.dirname(os.path.abspath(inspect.getfile(inspect.currentframe())))
parentdir = os.path.dirname(currentdir)
sys.path.insert(0, parentdir + '/libs')
import htmlement as html
import requests
import re
import json
#import urllib
#from urlparse import urlparse, parse_qs
#import os.path


def vpLog(msg):
    """
    logFilename = 'verpelis.log'
    fh = None
    if os.path.isfile(logFilename):
        fh = open(logFilename, 'a')
    else:
        fh = open(logFilename, 'w')
    fh.write(msg + "\n")
    fh.close()
    """
    #print 'VERPELIS LOGGING:'
    print msg

class VerPelis:
    
    url = None
    responseHeaders = None
    name = 'VerPelis'
    pageSteps = 2
    
    def __init__(self):
        self.url = 'http://ver-pelis.tv'
        
    def webRequest(self, url, type='GET', data={}, returnType='tree', parameters=None):
        page = None
        #print 'Doing {0} => {1}'.format(type, returnType)
        if type == 'POST':
            print data
            page = requests.post(url, data)
        elif type == 'PUT':
            page = requests.put(url)
        else:
            page = requests.get(url, parameters)

        if returnType == 'tree':
            #print 'RETURNING (TREE)'
            if page is None or page.content is None:
                return None
            #vpLog("Tree");
            #vpLog('Encoding: {0}'.format(page.encoding))
            rawHtml = page.content.strip()  # decode('UTF-8')
            #vpLog(rawHtml)
            tree = None
            if rawHtml.find('<body') == -1:
                rawHtml = '<html><head><title>Dummy HTML</title></head><body>{0}</body></html>'.format(rawHtml)
                #tree = html.fragment_fromstring(rawHtml, create_parent='div')
                #tree = html.fromstring(rawHtml)
                tree = html.fromstring(rawHtml)
            else:
                #tree = html.fromstring(rawHtml)
                tree = html.fromstring(rawHtml)
            return tree

        return page.content


    def strip_tags(self, value):
        """Returns the given HTML with all tags stripped."""
        return re.sub(r'<[^>]*?>', '', unicode(value))


    def stripSpecialChars(self, _string_):
        return re.sub(r'[^\x00-\x7f\t\r\n]', r'', _string_).strip()


    def getCategories(self):
        categories = []
        tree = self.webRequest(self.url)
        menuNode = tree.findall('.//ul[@id="main_menu"]')

        categoriesListNode = menuNode[0].findall('.//li[@class="normal_menu_item"][6]/ul/li')

        for item in categoriesListNode:
            categoryNode = item.findall('.//a')[0]
            if categoryNode is None:
                continue
            category = {
                'name': self.stripSpecialChars(unicode(categoryNode.text)),
                'url': categoryNode.get('href')
            }
            #print category['name']
            categories.append(category)

        return categories


    def getMovieData(self, url):
        data = {'excerpt': '','genre': '', 'actors': ''}
        return data
        '''
        tree = webRequest(url)

        excerptNode = tree.xpath('//div[contains(@class, "excerpt")]/p')
        genreNode = tree.xpath('//div[contains(@class, "specs_left")]/p')

        if len(excerptNode) == 2:
            data['excerpt'] = strip_tags(unicode(html.tostring(excerptNode[1])))
            data['excerpt'] = stripSpecialChars(data['excerpt'])
        if len(genreNode) > 0:
            data['genre'] = strip_tags(unicode(html.tostring(genreNode[0])))
            data['genre'] = stripSpecialChars(data['genre'])
            data['actors'] = stripSpecialChars(strip_tags(unicode(html.tostring(genreNode[1]))))
        return data
        '''


    def getLatestMovies(self, page=1):
        tree = self.webRequest(self.url)
        _list_ = self.findMovies(tree)
        #print _list_
        '''
        movies = tree.findall('.//div[@class="item_movie"]')
        for movie in movies:
            ##get image
            image_url = movie.findall('./a/img[1]')[0].get('src')
            link = movie.findall('./h3/a')[0]
            #print link
            title = link.text.strip()
            url = link.get('href')
            data = getMovieData(url)
            item = {
                'title': title,
                'url': url,
                'image_url': image_url,
                'excerpt': data['excerpt'],
                'genre': data['genre'],
                'quality': quality
            }
            _list_.append(item)
        '''
        return _list_


    def findMovies(self, tree):
        _list_ = []
        movies = tree.findall('.//div[@class="item_movie"]')
        #print movies
        for movie in movies:
            ##get image
            image_url = movie.findall('./a/img[1]')[0].get('src')
            link = movie.findall('./h3/a')[0]
            quality = unicode(movie.findall('./a/span[@class="res"]')[0].text)\
                        .encode('utf-8')\
                        .strip()
            #print quality
            #print link
            title = link.text.strip()
            url = link.get('href')
            item = {
                'title': title,
                'url': url,
                'image_url': image_url,
                'genre': '',
                'quality': quality,
                'year': '',
                'actors': '',
                'description': ''
            }
            ##print item
            _list_.append(item)
        #print _list_
        #return _list_
        return {'movies': _list_, 'pages': 1}


    def getCategoryMovies(self, url, page=1):
        tree = self.webRequest(url)
        return self.findMovies(tree)

    def searchMovie(self, keyword):
        vpLog("Searching movie keyword: " + keyword)
        searchUrl = self.url + '/ver/buscar?s=' + keyword
        vpLog("Search Url: " + searchUrl)
        tree = self.webRequest(searchUrl, returnType='tree')
        #print tree
        _list_ = self.findMovies(tree)

        return _list_


    def getMovieServers(self, url):
        vpLog('Getting Movie Servers')
        vpLog(url)
        tree = self.webRequest(url)
        #print html.tostring(tree)
        theScript = None
        scripts = tree.findall('.//body/div[@id="wrapper"]/script')
        for script in scripts:
            if script is None or script.text is None:
                continue
            # if script.text.find('cargar_video.php') != -1:
            if script.text.find('api.php') != -1:
                theScript = script.text
                break
        #print theScript
        result = re.search(r"id:\s*(\d+),", theScript)
        videoId = result.group(1)
        result1 = re.search(r"slug:\s*'(.*)',", theScript)
        videoSlug = result1.group(1)
        apiUrl = self.url + \
                    '/core/api.php?id={0}&slug={1}'.format(videoId, videoSlug)
        _json_ = self.webRequest(apiUrl, 'GET', None, 'text')
        #print _json_
        listServers = json.loads(_json_)
        servers = []
        for serverName in listServers['lista']:
            #print serverName
            server = listServers['lista'][serverName]
            if server is None:
                continue
            #print server
            for lang in server:
                '''
                if lang is None or lang.find('subtitulos') != -1 \
                    or type(server[lang]) is not list:
                    continue
                '''
                if type(server[lang]) is list:
                    for item in server[lang]:
                        _server_ = {
                            'name': item['servidortxt'],
                            'lang': lang,
                            'videoId': item['videoid'],
                            'quality': item['calidad'],
                            'slug': videoSlug
                        }
                        servers.append(_server_)

        return servers


    def getMovieLink(self, server):
        '''
        finalVideoUrl = '{0}/ajax/video.php?id={1}&slug={2}&quality={3}'.format(
                                    site_url,
                                    server['videoId'],
                                    server['slug'],
                                    server['quality']
        )
        '''
        finalVideoUrl = u'{0}/ajax/verpelis.php?id={1}&slug={2}'.format(
                            self.url,
                            server['videoId'],
                            server['slug'])
        #print finalVideoUrl
        header = {'allow_redirects': 'True'}
        rawHtml = self.webRequest(finalVideoUrl, 'GET', {}, 'text', header)
        #print rawHtml
        result = re.search(r"location=\"(.*)\"", rawHtml)
        serverVideoUrl = u'{0}'.format(result.group(1)).strip()
        #print 'Server Video URL: {0}'.format(serverVideoUrl)
        #urlComs = urlparse(serverVideoUrl)
        #params = parse_qs(urlComs.query)
        #print params
        #serverUrl = params['playLink'][0].split('?')[0].replace('/e/', '?v=')
        #serverHtml1 = webRequest(serverVideoUrl, 'GET', {}, 'text', header)
        #print serverHtml1
        #result = re.search(r"<iframe.*src=\"(.+?)\"\s+", serverHtml1)
        #serverUrl = result.group(1)
        #print serverUrl
        #serverHtml = webRequest(serverUrl, 'GET', {}, 'tree', header)
        serverHtml = self.webRequest(serverVideoUrl, 'GET', {}, 'tree', header)
        #print MET.tostring(serverHtml)
        videoElement = serverHtml.findall('.//video/source')
        if videoElement is None or not videoElement:
            print 'Video url not found'
            return None

        videoUrl = videoElement[0].get('src')
        return videoUrl
        
    def getMovieVideoFile(self, videoUrl):
        
        return ''
    def getMovieVideoFiles(self, videoUrl):
        servers = self.getMovieServers(videoUrl)
        files = []
        for srv in servers:
            if 'Rapid' in srv['name']:
                files.append({
                    'name': '{0} - {1}'.format(srv['slug'], srv['lang']), 
                    'quality': srv['quality'], 'url': self.getMovieLink(srv)})
        # print files
        return files

if __name__ == '__main__':
    invalidAction = 'Invalid action, please use\n FanPelis.py [download|link|search|categories] [url]'
    # print sys.argv
    if len(sys.argv) <= 1:
        print invalidAction
        quit()
    action = sys.argv[1]

    if (action in ['download','link','search', 'categories']) == False:
        print invalidAction
        quit()
    if len(sys.argv) <= 2:
        print invalidAction
        quit()

    url = sys.argv[2]
    print 'Initializing provider....'
    obj = VerPelis()
    if action in ['link', 'download']:
        servers = obj.getMovieServers(url)
        print servers
        i = 0
        for srv in servers:
            print '{0}. {1}({2}) => {3}'.format(i, srv['name'], srv['lang'], srv['quality'])
            i = i + 1

        link = obj.getMovieLink(servers[3])
        print link
    elif action == 'search':
        movies = obj.searchMovie(url)
        option = None
        #while option is not None and (option) != 'q':
        _i_ = 1
        for mov in movies:
            print u'{0}. {1} => {2}'.format(_i_, mov['title'], mov['url'])
            _i_ += 1
        print 'q. Quit'
        option = raw_input('Select an option: ')
        if int(option) <= len(movies):
            servers = obj.getMovieServers(movies[int(option) - 1]['url'])
            _i_ = 1
            for srv in servers:
                print '{0}. {1}({2}) => {3}'.format(_i_, srv['name'], srv['lang'], srv['quality'])
                _i_ += 1
            print 'q. Quit'
            option = raw_input('Select an option: ')
            print obj.getMovieLink(servers[int(option) - 1])
    elif action == 'categories':
        print obj.getCategories()
