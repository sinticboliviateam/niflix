# -*- coding: utf-8 -*-
import os,sys,inspect
currentdir = os.path.dirname(os.path.abspath(inspect.getfile(inspect.currentframe())))
parentdir = os.path.dirname(currentdir)
sys.path.insert(0, parentdir + '/libs')
import htmlement as html
import requests
import re
import json

class ProviderBase:
    url = None
    responseHeaders = None
    responseCookies = None
    lastUrl         = None
    name = 'FanPelis'
    pageSteps = 2
    
    def webRequest(self, url, type='GET', data={}, returnType='tree', parameters=None, headers=None, cookies=None):
        page = None
        
        if headers is None:
            headers = {'Referer': self.lastUrl}
        else:
            headers['Referer'] = self.lastUrl
            
        #print 'Doing {0} => {1}'.format(type, returnType)
        if type == 'POST' or type == 'PUT':
            page = requests.post(url, data, headers=headers, cookies=self.responseCookies, allow_redirects=True)
        else:
            page = requests.get(url, parameters, headers=headers, cookies=self.responseCookies, allow_redirects=True)
            
        self.responseHeaders    = page.headers
        self.responseCookies    = page.cookies
        
        if returnType == 'tree':
            #print 'RETURNING (TREE)'
            if page is None or page.content is None:
                return None
            #vpLog("Tree");
            #vpLog('Encoding: {0}'.format(page.encoding))
            rawHtml = page.content.strip()  # decode('UTF-8')
            #vpLog(rawHtml)
            tree = None
            if rawHtml.find('<body') == -1:
                rawHtml = '<html><head><title>Dummy HTML</title></head><body>{0}</body></html>'.format(rawHtml)
                #tree = html.fragment_fromstring(rawHtml, create_parent='div')
                #tree = html.fromstring(rawHtml)
                tree = html.fromstring(rawHtml)
            else:
                #tree = html.fromstring(rawHtml)
                tree = html.fromstring(rawHtml)
            return tree

        return page.content
        
    def normalizeText(self, text):

        normalizedText = ''
        for i in text.strip():
            #print u'{0} => {1}'.format(i, ord(i))
            char = ''
            try:
                if ord(i) < 128:
                    char = u'{0}'.format(i.decode('ascii'))
                else:
                    char = u'{0}'.format(i.decode('utf-8'))

                normalizedText = u'{0}{1}'.format(normalizedText, char)
            except:
                normalizedText = u'{0}{1}'.format(normalizedText, '')
                #print 'Error processing => '
                #print i
                #print ord(i)

        return normalizedText
    def getRapidvideoFiles(self, url):
        videoTree = self.webRequest(url)
        videoFiles = videoTree.findall('.//video/source')
        
        return videoFiles
        
    def getRapidvideoFile(self, url):
        videoFiles = self.getRapidvideoFiles(url)
        videoFile = videoFiles[0].get('src')
        
        return videoFile
