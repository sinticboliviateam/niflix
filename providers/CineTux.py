# -*- coding: utf-8 -*-
import os,sys,inspect
currentdir = os.path.dirname(os.path.abspath(inspect.getfile(inspect.currentframe())))
parentdir = os.path.dirname(currentdir)
sys.path.insert(0, parentdir + '/libs')
import htmlement as html
import requests
import re
import json
from ProviderBase import ProviderBase

class CineTux(ProviderBase):
    
    def __init__(self):
        self.url = 'https://www.cinetux.to'
        self.name = 'CineTux'
    
    def extractMovies(self, containerNode):
        movies = []
        movieNodes = containerNode.findall('.//div[@id="contenedor"]//div[@class="items"]/article');
        
        for movieNode in movieNodes:
            imgNode = movieNode.find('./div[@class="poster"]/img')
            dataNode = movieNode.find('./div[@class="data"]')
            anchorNode = movieNode.find('./a')
            
            movie = {
                'title': u'{0}'.format(imgNode.get('alt')),
                'genre': '',
                'year': dataNode.find('./span[1]/a').text,
                'actors': '',
                'quality': '',
                'description': '',
                'image_url': imgNode.get('data-lazy-src'),
                'url': anchorNode.get('href')
            }
            movies.append(movie)
            
        return movies
        
    def getLatestMovies(self, page=1):
        url = self.url
        regex = './/div[@id="contenedor"]//div[@class="items"]/article'
        if page > 1:
            url += '/pelicula/page/{0}/'.format(page)
            regex = './/div[@id="contenedor"]//div[@id="archive-content"]/article'
        
        tree = self.webRequest(url)
        
        movies = []
        movieNodes = tree.findall(regex)
        
        for movieNode in movieNodes:
            imgNode = movieNode.find('./div[@class="poster"]/img')
            dataNode = movieNode.find('./div[@class="data"]')
            anchorNode = movieNode.find('./a')
            yearNode = dataNode.find('./span[1]/a')
            movie = {
                'title': u'{0}'.format(imgNode.get('alt')),
                'genre': '',
                'year': yearNode.text if yearNode is not None else '',
                'actors': '',
                'quality': '',
                'description': '',
                'image_url': imgNode.get('data-lazy-src'),
                'url': anchorNode.get('href')
            }
            movies.append(movie)
        
        
        return {'movies': movies, 'pages': 100}
        
    def getCategories(self):
        tree = self.webRequest(self.url)
        liNodes = tree.findall('.//div[@id="genez"]/ul/li');
        cats = []
        
        for li in liNodes:
            anchor = li.find('./a')
            url = self.url + anchor.get('href')
            name = u'{0}'.format(self.normalizeText(anchor.text))
            cats.append({'name': name, 'url': url})
            
        return cats
    
    def getCategoryMovies(self, categoryUrl, page=1):
        url = categoryUrl
        if page > 1:
            url += 'page/{0}'.format(page)
            
        tree = self.webRequest(url)
        movies = self.extractMovies(tree)
             
        return {'movies': movies, 'pages': 100}
        
    def getMovieServers(self, movieUrl):
        tree = self.webRequest(movieUrl)
        #query = './/div[@class="fix-table"]//td//a/img[@alt="rapidvideo.com"]/..'
        query = './/div[@class="fix-table"]//tbody/tr'
        
        tableRows = tree.findall(query)
        print tableRows
        servers = []
        for tr in tableRows:
            serverNode = tr.findall('./td//a/img[@alt="rapidvideo.com"]/..')
            if serverNode is None or len(serverNode) <= 0:
                continue
            qualityNode = tr.findall('./td[3]')[0]
            langNode = tr.findall('./td[4]')[0]
            #print type(qualityNode).__name__
            quality = u"".join(qualityNode.itertext())
            language = u"".join(langNode.itertext())
            srv = {
                'name': 'Rapidvideo',
                'quality': self.normalizeText(quality),
                'lang': language,
                'url': serverNode[0].get('href')
            }
            servers.append(srv)
        
        return servers
        
    def getMovieLink(self, server, qualities=False):
       
        tree = self.webRequest(server['url'])
        anchor = tree.find('.//a[@id="link"]')
        rapidVideoUrl = anchor.get('href')
        linkUrl = self.getRapidvideoFile(rapidVideoUrl)
        return linkUrl
    
    def getMovieVideoFile(self, movieUrl, qualities=False):
        servers = self.getMovieServers(movieUrl)
        
        return self.getMovieLink(server[0])
         
        
if __name__ == '__main__':
    invalidAction = 'Invalid action, please use\n FanPelis.py [download|link|search|categories] [url]'
    # print sys.argv
    if len(sys.argv) <= 1:
        print invalidAction
        quit()
    action = sys.argv[1]

    if (action in ['download','link','search', 'categories', 'latest']) == False:
        print invalidAction
        quit()
    if len(sys.argv) <= 2:
        print invalidAction
        quit()

    url = sys.argv[2]
    print 'Initializing provider with action => {0}....'.format(action)
    obj = CineTux()
    
    if action in ['link', 'download']:
        servers = obj.getMovieServers(url)
        print servers
        i = 0
        for srv in servers:
            print u'{0}. {1}({2}) => {3}'.format(i, srv['name'], srv['lang'], srv['quality'])
            i = i + 1

        link = obj.getMovieLink(servers[0])
        print link
    elif action == 'latest':
        print obj.getLatestMovies()
    elif action == 'search':
        movies = obj.searchMovie(url)
        option = None
        #while option is not None and (option) != 'q':
        _i_ = 1
        for mov in movies:
            print u'{0}. {1} => {2}'.format(_i_, mov['title'], mov['url'])
            _i_ += 1
        print 'q. Quit'
        option = raw_input('Select an option: ')
        if int(option) <= len(movies):
            servers = obj.getMovieServers(movies[int(option) - 1]['url'])
            _i_ = 1
            for srv in servers:
                print '{0}. {1}({2}) => {3}'.format(_i_, srv['name'], srv['lang'], srv['quality'])
                _i_ += 1
            print 'q. Quit'
            option = raw_input('Select an option: ')
            print obj.getMovieLink(servers[int(option) - 1])
    elif action == 'categories':
        print obj.getCategories()

