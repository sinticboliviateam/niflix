# -*- coding: utf-8 -*-
import os,sys,inspect
currentdir = os.path.dirname(os.path.abspath(inspect.getfile(inspect.currentframe())))
parentdir = os.path.dirname(currentdir)
sys.path.insert(0, parentdir + '/libs')
import requests
#from lxml import html
#from libs import htmlement as html
#from libs import htmlement as html
import htmlement as html
import re
#import HTMLParser
import math
from ProviderBase import ProviderBase

class FanPelis(ProviderBase):

    url = None
    responseHeaders = None
    name = 'FanPelis'
    pageSteps = 2

    def __init__(self):
        self.url = 'http://fanpelis.com'

    def normalize(self, text):

        normalizedText = ''
        for i in text.strip():
            #print u'{0} => {1}'.format(i, ord(i))
            char = ''
            try:
                if ord(i) < 128:
                    char = u'{0}'.format(i.decode('ascii'))
                else:
                    char = u'{0}'.format(i.decode('utf-8'))

                normalizedText = u'{0}{1}'.format(normalizedText, char)
            except:
                normalizedText = u'{0}{1}'.format(normalizedText, '')
                #print 'Error processing => '
                #print i
                #print ord(i)

        return normalizedText
        #h = HTMLParser.HTMLParser()
        #return h.unescape(unicode(text.encode('utf-8')))
        #return ''.join([i if ord(i) < 128 else ' ' for i in text]).strip().split()[0]

    """
    Sends a web requests
    """
    """
    def webRequest(self, url, type='GET', data={}, returnType='tree',
                    parameters=None, headers=None, cookies=None):

        page = None
        if type == 'POST':
            page = requests.post(url, data, headers=headers, cookies=cookies)
        elif type == 'PUT':
            page = requests.put(url, headers=headers, cookies=cookies)
        else:
            page = requests.get(
                        url,
                        #parameters,
                        headers=headers,
                        cookies=cookies
            )
        self.response = page
        #save response headers
        self.responseHeaders = page.headers
        if returnType == 'tree':
            if page is None or page.content is None:
                return None
            rawHtml = page.content.strip()  # decode('UTF-8')
            #print rawHtml
            tree = None
            if rawHtml.find('<body') == -1:
                rawHtml = '<html><head><title>Dummy HTML</title></head><body>{0}</body></html>'.format(rawHtml)
                #tree = html.fragment_fromstring(rawHtml, create_parent='div')
                tree = html.fromstring(rawHtml)
            else:
                tree = html.fromstring(rawHtml)
            return tree

        return page.content
    """
    def getCategories(self):
        items = []
        tree = self.webRequest(self.url)
        links = tree.findall('.//ul[@id="menu-main-menu"]/li[2]//ul/li/a')

        for link in links:
            item = {
                'name': u'{0}'.format(link.text.strip()),
                'url': link.get('href')
            }
            items.append(item)

        return items

    def getLatestMovies(self, page=1):
        tree = self.webRequest(self.url)
        #wraps = tree.findall('.//div[contains(@class, "movies-list-wrap")]')
        wraps = tree.findall('.//div[@id="main"]/div[@class="container"]');

        movies = self.extractMovies(wraps[0])

        return {'movies': movies, 'pages': 1}

    def searchMovie(self, keyword):
        url = u'{0}/?s={1}'.format(self.url, keyword)
        # print url
        tree = self.webRequest(url)
        movies = self.extractMovies(tree)

        return movies

    def getCategoryMovies(self, categoryUrl, page=1):

        movies = []
        totalPages = None
        _page_ = 1
        if page > 1:
            _page_ = int(self.pageSteps) * int(page) - 1

        for step in range(int(self.pageSteps)):
            print u'Doing step: {0}'.format(step)

            _url_ = u'{0}/page/{1}'.format(categoryUrl, _page_)
            tree = self.webRequest(_url_)

            if totalPages is None:
                #get total pages
                nodeLastPage = tree.findall('.//ul[@class="pagination"]/li[last()]/a')[0]
                lastPageUrl = nodeLastPage.get('href')
                res = re.search('(\d+)', lastPageUrl)
                totalPages = int(res.group(0))
                totalPages = int(math.ceil(totalPages / self.pageSteps))

            movies += self.extractMovies(tree, 'ml-item MovItem')
            _page_ += 1

        if totalPages is None:
            totalPages = 1

        return {'movies': movies, 'pages': totalPages}

    def extractMovies(self, tree, movieClass='ml-item'):
        movies = []
        #items = tree.findall('.//div[contains(@class, "ml-item")]')
        items = tree.findall('.//div[@class="{0}"]'.format(movieClass))
        for item in items:
            anchor = item.findall('./a')[0]
            # print html.tostring(anchor)
            title = self.normalize(anchor.get('oldtitle')).strip()
            #print title
            url = anchor.get('href')
            try:
                quality = ''
                qualityNode = anchor.findall('./span[@class="mli-quality"]')

                if qualityNode is not None and len(qualityNode) > 0:
                    quality = self.normalize(qualityNode[0].text)

                nodeImage = anchor.findall('./img')[0]
                image_url = nodeImage.get('src')
                if image_url is None:
                    image_url = nodeImage.get('data-original')

                description = ''
                info = item.findall('./div[@id="hidden_tip"]')[0]
                pNodes = info.findall('.//p')

                if len(pNodes) > 1 and pNodes[1].text is not None:
                    description = self.normalize(pNodes[1].text)

                year = u'Unknown'

                #'''
                nodeYear = info.findall('.//div[@class="jt-info"]/a')
                if len(nodeYear) > 0:
                    #print nodeYear
                    year = self.normalize(nodeYear[0].text)
                #'''
                movie = {
                    'title': title,
                    'url': url,
                    'image_url': image_url,
                    'year': year,
                    'duration': u'',
                    'quality': quality,
                    'description': description,
                    'genre': u'',
                    'actors': u''
                }
                movies.append(movie)
            except:
                print u'Error processing: {0}'.format(self.normalize(title))
                e = sys.exc_info()[0]
                print e

        return movies

    def getMovieQualities(self, movieUrl):
        items = []

        tree = self.webRequest(movieUrl)
        title = self.normalize(tree.findall('.//title')[0].text)

        #links = tree.findall('.//a[contains(@href, "' + movieUrl + '")]')
        links = tree.findall('.//a')
        for link in links:
            href = link.get('href')

            if href is None or href.find(movieUrl) == -1:
                continue
            item = {
                'name': title,
                'quality': u'{0}'.format(self.normalize(link.find('./div').text)),
                'url': href
            }
            items.append(item)

        return items
        
    def getMovieServers(self, movieUrl):
        print 'Getting movie servers: {0}'.format(movieUrl)
        tree = self.webRequest(movieUrl)
        nodeTitle = tree.find('.//head/title');
        #print nodeTitle
        movie_name_parts = nodeTitle.text.split('-')
        movie_name = ''
        if len(movie_name_parts) > 0 :
            movie_name = movie_name_parts[0].strip()
            
        inputMovieId = tree.find('.//form/input[@name="id"]')
        moviePostId = inputMovieId.get('value')
        tree = self.webRequest('https://fanpelis.net/wp-admin/admin-ajax.php', 'POST', 
                                {'action':'movie_player','foobar_id':moviePostId}, 'tree')
        # query = './/div[@class="btn-group btn-group-justified embed-selector"]/a/span/img[@alt="https://Rapidvideo"]/../..'
        query = './/ul[@class="idTabs"]//li/div/a'
        serverNodes = tree.findall(query)
        
        if serverNodes is None or len(serverNodes) <= 0:
            return None
        servers = []
        for srv in serverNodes:
            if 'Rapid' not in srv.text:
                continue
                
            servers.append({
                'name': srv.text,
                'link': srv.get('data-url'),
                'movie_name': movie_name
            })
        
        return servers
        
    def getMovieVideoFiles(self, movieUrl):
        print 'Getting movie files: {0}'.format(movieUrl)
        servers = self.getMovieServers(movieUrl)
        
        if servers is None or len(servers) <= 0:
            return []
        
        sources = self.getRapidvideoFiles(servers[0]['link'])
        files = []
        for src in sources:
            files.append({
                'name': servers[0]['movie_name'],
                'quality': src.get('label'),
                'url': src.get('src')
            })
        return files

if __name__ == '__main__':
    invalidAction = 'Invalid action, please use\n FanPelis.py [download|link|search] [url]'
    # print sys.argv
    if len(sys.argv) <= 1:
        print invalidAction
        quit()
    action = sys.argv[1]

    if (action in ['download','link','search', 'latest', 'category_movies']) == False:
        print invalidAction
        quit()
    if len(sys.argv) <= 2:
        print invalidAction
        quit()

    url = sys.argv[2]
    print 'Initializing provider....'
    obj = FanPelis()

    if action in ['link', 'download']:
        servers = obj.getMovieServers(url)
        print servers
        files = obj.getMovieVideoFiles(servers[0]['link'])
        print files
        
    elif action == 'search':
        movies = obj.search(url)
        option = None
        #while option is not None and (option) != 'q':
        _i_ = 1
        for mov in movies:
            print '{0}. {1} => {2}'.format(_i_, mov['title'], mov['url'])
            _i_ += 1
        print 'q. Quit'
        option = raw_input('Select an option: ')
        if int(option) <= len(movies):
            rapidVideoUrl = obj.getMovieVideoFile(movies[int(option) - 1]['url'])
            qualities = obj.getMovieQualities(rapidVideoUrl)
            for q in qualities:
                print '{0} => {1}'.format(q['quality'], q['url'])
    elif action == 'latest':
        print obj.getLatestMovies()
    elif action == 'category_movies':
        print obj.getCategoryMovies(url)
'''
if __name__ == '__main__ ':
    #print obj.getCategories()
    #
    #movies = obj.getLatestMovies()
    #movies = obj.getCategoryMovies('http://fanpelis.com/genre/ciencia-ficcion')
    #print movies
    #qualities = obj.getMovieQualities('https://www.rapidvideo.com/v/FWZ435B2B9')
    #print qualities
    rapidVideoUrl = obj.getMovieVideoFile('http://fanpelis.com/latidos-en-la-oscuridad/')
    print rapidVideoUrl
    qualities = obj.getMovieQualities(rapidVideoUrl)
    print obj.getRapidvideoFile(qualities[0]['url'])
'''
