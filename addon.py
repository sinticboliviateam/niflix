# -*- coding: utf-8 -*-
# encoding: utf-8
import sys
import xbmc
import xbmcgui
import xbmcplugin
import urllib
import urlparse
import importlib
from NiFlix import NiFlix

_addonName_ = 'NiFlix'

win = xbmcgui.Window()
baseUrl = sys.argv[0]
addon_handle = int(sys.argv[1])
queryString = sys.argv[2][1:]
args = urlparse.parse_qs(queryString)

##instance object
prov = NiFlix()

thumbnailView = 500
wallView = 54
infoWallView = 597
xbmcplugin.setContent(addon_handle, 'movies')
xbmc.executebuiltin('Container.SetViewMode(%d)' % wallView)
providerObj = None


def build_url(query):
    return baseUrl + '?' + urllib.urlencode(query)


providers = [
    {
        'name': 'FanPelis',
        'icon': 'DefaultVideo.png',
        'url': build_url({'action': 'provider', 'name': 'FanPelis'})
    },
    {
        'name': 'VerPelis',
        'icon': 'providers/ver-pelis.png',
        'url': build_url({'action': 'provider', 'name': 'VerPelis'})
    },
    {
        'name': 'CineTux',
        'icon': './providers/ver-pelis.png',
        'url': build_url({'action': 'provider', 'name': 'CineTux'})
    }
]


def loadProvider(name):
    print 'Looking for provider: ' + name
    provider = None
    for prov in providers:
        print prov['name']
        if prov['name'] == name:
            print 'Provider found:'
            print prov
            provider = prov
            break

    if provider is not None:
        providerModule = importlib.import_module(
            '.' + provider['name'],
            'providers'
        )
        providerClass = getattr(providerModule, provider['name'])
        provider = providerClass()

    return provider


def mainMenu():

    for provider in providers:
        li = xbmcgui.ListItem(provider['name'], iconImage=provider['icon'], thumbnailImage=provider['icon'])
        li.setProperty('fanart_image', provider['icon'])
        xbmcplugin.addDirectoryItem(
            handle=addon_handle,
            url=provider['url'],
            listitem=li,
            isFolder=True
        )

    xbmcplugin.endOfDirectory(addon_handle)


def mainMenuProvider(provider):
    options = [
        {'label': 'Search', 'icon': 'DefaultVideo.png',
            'url': build_url({'action': 'search', 'name': provider.name})},
        {'label': 'Latest Movies', 'icon': 'DefaultVideo.png',
            'url': build_url({'action': 'latest', 'name': provider.name})}
    ]
    for op in options:
        li = xbmcgui.ListItem(op['label'], iconImage=op['icon'])
        xbmcplugin.addDirectoryItem(handle=addon_handle, url=op['url'],
            listitem=li, isFolder=True)

    categories = provider.getCategories()

    for _category_ in categories:
        category_url = build_url({
            'action': 'category',
            'name': provider.name,
            'url': _category_['url']
        })
        _li_ = xbmcgui.ListItem(_category_['name'],
                                iconImage='DefaultVideo.png'
        )
        xbmcplugin.addDirectoryItem(handle=addon_handle,
                                    url=category_url,
                                    listitem=_li_,
                                    isFolder=True)

    xbmcplugin.endOfDirectory(addon_handle)


def showLatest(provider, page=1):
    result = provider.getLatestMovies(page)
    listMovies(result['movies'], page, result['pages'])


def showServers(movieUrl):
    _list_ = []
    servers = prov.getMovieServers(movieUrl)

    for server in servers:
        serverName = u"{0} - {1}".format(
                        unicode(server['name'].encode('utf-8')),
                        unicode(server['lang'])
        )
        _list_.append(serverName)

    index = xbmcgui.Dialog().select(
        '{0} - Select Movie Server - {1}'.format(_addonName_, ''),
        _list_)

    if index == -1:
        return True
    _server_ = servers[index]
    videoUrl = prov.getMovieLink(_server_)
    ##play de movie
    playVideo(videoUrl['link'])


def showVideoFiles(videoUrl):
    print videoUrl
    files = []
    if providerObj.name in ['CineTux']:
        files = providerObj.getMovieServers(videoUrl)
    else:
        files = providerObj.getMovieVideoFiles(videoUrl)
        
    _list_ = []
    for _file_ in files:
        _list_.append(u'{0} {1}'.format(_file_['name'], _file_['quality']))

    index = xbmcgui.Dialog().select(
        '{0} - Select Movie Quality {1}'.format(_addonName_, ''),
        _list_)

    if index == -1:
        return True

    _quality_ = files[index]
    print _quality_
    videoUrl = None;
    if providerObj.name == 'CineTux':
        videoUrl = providerObj.getMovieLink(_quality_)
    else:
        videoUrl = _quality_['url']
    ##play de movie
    playVideo(videoUrl)


def playVideo(videoUrl):
    if videoUrl is None:
        return False

    playItem = xbmcgui.ListItem(path=videoUrl)
    xbmcplugin.setResolvedUrl(addon_handle, True, listitem=playItem)


def listMovies(movies, currentPage=1, totalPages=None, action='latest',
                url=None):
    if int(currentPage) > 1:
        li = xbmcgui.ListItem('Previous Page', iconImage='', thumbnailImage='')
        args = {
            'action': action,
            'page': int(currentPage) - 1,
            'name': providerObj.name
        }
        if url is not None:
            args['url'] = url

        _url_ = build_url(args)
        xbmcplugin.addDirectoryItem(handle=addon_handle, url=_url_, listitem=li,
            isFolder=True)

    for movie in movies:
        #description = movie['description']
        plot = u"\nGenre: {0}\nYear: {1}\nActors: {2}\nQuality:{3}".format(
                        movie['genre'],
                        movie['year'],
                        movie['actors'],
                        movie['quality']
                    )
        li = xbmcgui.ListItem(movie['title'],
            iconImage=movie['image_url'],
            thumbnailImage=movie['image_url']
        )
        li.setProperty('fanart_image', movie['image_url'])
        li.setProperty('IsPlayable', 'true')
        li.setInfo('video', {
                'title': movie['title'],
                'genre': movie['genre'],
                'year': movie['year'],
                'tagline': movie['description'],
                'plot': plot,
                'cast': movie['actors'].split(',')
        })
        _url_ = build_url({
            'action': 'list_files',
            'video': movie['url'],
            'name': providerObj.name
        })
        xbmcplugin.addDirectoryItem(handle=addon_handle, url=_url_, listitem=li)

    if totalPages is not None and int(totalPages) > 1:
        li = xbmcgui.ListItem('Next Page', iconImage='', thumbnailImage='')
        args = {
            'action': action,
            'page': int(currentPage) + 1,
            'name': providerObj.name
        }
        if url is not None:
            args['url'] = url
        _url_ = build_url(args)
        xbmcplugin.addDirectoryItem(handle=addon_handle, url=_url_,
            listitem=li,
            isFolder=True)

    xbmcplugin.endOfDirectory(addon_handle)
    xbmc.executebuiltin('Container.SetViewMode({0})'.format(wallView))


def getQVar(varName, default=None):
    var = args.get(varName, None)
    if var is None:
        return default

    result = var[0] if var is not None else default

    return result

#check action
#action = args.get('action', None)
#action = action[0] if action is not None else 'default'
action = getQVar('action')
provider = getQVar('name')
providerObj = loadProvider(provider) if provider is not None else None

if action == 'provider':
    mainMenuProvider(providerObj)
if action == 'latest':
    page = getQVar('page', 1)
    showLatest(providerObj, page=page)
elif action == 'category':
    page = getQVar('page', 1)
    category_url = getQVar('url')
    print category_url
    result = providerObj.getCategoryMovies(category_url, page)
    listMovies(
        result['movies'],
        page,
        result['pages'],
        'category',
        category_url
    )
elif action == 'list_servers':
    video = args.get('video', None)
    video = video[0] if video is not None else 'empty'
    if video == 'empty':
        xbmcgui.Dialog().ok('PelisGratis - Error', action,
            'The movie link is invalid')
    else:
        showServers(video)
elif action == 'list_files':
    videoUrl = urllib.unquote(getQVar('video'))
    showVideoFiles(videoUrl)
elif action == 'search':
    keyword = xbmcgui.Dialog().input(
        '{0} - Search Movie'.format(_addonName_), ''
    )
    movies = providerObj.searchMovie(keyword)
    listMovies(movies)
elif action == 'play':
    video = args.get('video', None)
    video = video[0] if video is not None else 'empty'
    #xbmcgui.Dialog().ok('Ver Pelis', action, video)
    if video == 'empty':
        xbmcgui.Dialog().ok('Ver Pelis - Error', action, video)
    else:
        playVideo(video)
else:
    mainMenu()
