# -*- coding: utf-8 -*-
import json
import requests

class NiFlix:
    baseUrl = ''

    def __init__(self):
        self.baseUrl = 'http://sinticbolivia.net/temp/NifScraper.php'

    """
    Sends a web requests
    """
    def webRequest(self, url, type='GET', data=None, headers=None,
                    cookies=None):

        page = None
        if type == 'POST':
            page = requests.post(url, data, headers=headers, cookies=cookies)
        elif type == 'PUT':
            page = requests.put(url, headers=headers, cookies=cookies)
        else:
            page = requests.get(url, None, headers=headers, cookies=cookies)

        response = {
            'headers': page.headers,
            #'obj': page,
            'content': page.content
        }

        return response

    def getCategories(self):
        url = self.baseUrl + '?task=categories'
        response = self.webRequest(url)
        categories = json.loads(response['content'])
        return categories

    def getLatestMovies(self, page=1):
        url = self.baseUrl + '?task=latest'
        if page > 1:
            url = url + '&page={0}'.format(int(page))

        response = self.webRequest(url)
        movies = json.loads(response['content'])
        return movies

    def getCategoryMovies(self, url, page=1):
        endpoint = self.baseUrl + '?task=category_movies'
        if page > 1:
            endpoint = endpoint + '&page={0}'.format(int(page))

        response = self.webRequest(endpoint, 'POST', {'cat_url': url})
        category = json.loads(response['content'])
        return category

    def searchMovie(self, keyword):
        endpoint = self.baseUrl + '?task=search&keyword=' + keyword
        response = self.webRequest(endpoint)
        search = json.loads(response['content'])
        return search

    def getMovieServers(self, movieUrl):
         url = self.baseUrl + '?task=servers'
         response = self.webRequest(url, 'POST', {'url': movieUrl})

         return json.loads(response['content'])

    def getMovieLink(self, server):
        url = self.baseUrl + '?task=link'
        response = self.webRequest(url, 'POST', {'server_url': server['url']})

        return json.loads(response['content'])


#obj = NiFlix()
#movies = obj.getLatestMovies()
#print movies
#servers = obj.getMovieServers(movies['movies'][1]['url']);
#print servers
#link = obj.getMovieLink(servers[3]['url'])
#print link